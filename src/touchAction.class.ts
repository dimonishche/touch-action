// import 'hammerjs';
import { PanActions, PAN_ACTIONS } from './panActions.class';
import { SwipeActions, SWIPE_ACTIONS } from './swipeAction.class';
import { Observable, Subject } from 'rxjs';
const hammerLoader = require('bundle-loader!hammerjs');
declare let Hammer: any;

export interface SwipeConfig {
  velocity: number,
  direction: number
}

export interface PanConfig {
  threshold: number,
  direction: number
}

export enum ACTIONS {
  SWIPE_LEFT = SWIPE_ACTIONS.LEFT,
  SWIPE_RIGHT = SWIPE_ACTIONS.RIGHT,
  SWIPE_UP = SWIPE_ACTIONS.UP,
  SWIPE_DOWN = SWIPE_ACTIONS.DOWN,
  PAN_START = PAN_ACTIONS.START,
  PAN_LEFT = PAN_ACTIONS.LEFT,
  PAN_RIGHT = PAN_ACTIONS.RIGHT,
  PAN_UP = PAN_ACTIONS.UP,
  PAN_DOWN = PAN_ACTIONS.DOWN,
  PAN_END = PAN_ACTIONS.END,
  PAN_CANCEL = PAN_ACTIONS.CANCEL
}

export class TouchActionClass {

  private actionObservable: Subject<{ action: ACTIONS, value: number }> = new Subject();
  private readyState: Subject<boolean> = new Subject();
  private container: HTMLElement;
  private parent: HTMLElement;
  private hammerManager: HammerManager;
  private panActions: PanActions;
  private swipeActios: SwipeActions;
  private canSwipe: boolean = true;
  private enabled: boolean = true;
  private readonly UNRESPONSE_TIME = 200;

  constructor(container: HTMLElement, swipeConfig?: SwipeConfig, panConfig?: PanConfig, parent?: HTMLElement) {
    if (!window) return;

    this.container = container;
    this.parent = parent || this.container;
    hammerLoader((_hammer) => {
      Hammer = _hammer;

      this.createHammerObject();
      this.readyState.next(true);

      this.panActions = new PanActions(this.hammerManager, this.parent, panConfig);
      this.swipeActios = new SwipeActions(this.hammerManager, swipeConfig);

      this.listenPan();
      this.listenSwipe();
    });
  }

  public get actions(): Observable<{ action: ACTIONS, value: number }> {
    return this.actionObservable.asObservable();
  }

  public disableActions(): void {
    try {
      this.hammerManager.set({ enable: false });
      this.enabled = false;
    } finally { }
  }

  public enableActions(): void {
    this.hammerManager.set({ enable: true });
    this.enabled = true;
  }

  public onReady(): Observable<boolean> {
    return this.readyState.asObservable();
  }

  private createHammerObject(): void {
    Hammer.defaults.cssProps.userSelect = 'text';
    this.hammerManager = new Hammer(this.container);
  }

  private pushAction(action: number, value: number = null) {
    if (!this.canSwipe) return;

    if (action in SWIPE_ACTIONS) this.setSwipeUnresponse();

    this.actionObservable.next({ action, value });
  }

  private listenPan(): void {
    this.panActions.onPan.subscribe(event => {
      this.pushAction(event.action, event.value);
    })
  }

  private listenSwipe(): void {
    this.swipeActios.onSwipe.subscribe(event => {
      this.pushAction(event);
    })
  }

  private setSwipeUnresponse(): void {
    this.canSwipe = false;

    setTimeout(() => this.canSwipe = true, this.UNRESPONSE_TIME);
  }
}