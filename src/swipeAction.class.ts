import { Observable, Subject } from 'rxjs';

export enum SWIPE_ACTIONS {
  LEFT = 0,
  RIGHT = 1,
  UP = 2,
  DOWN = 3
}

export class SwipeActions {
  private swipeObservable: Subject<SWIPE_ACTIONS> = new Subject();
  private hammerManager: HammerManager;
  private swipeConfig: RecognizerOptions = { velocity: 0.3, direction: Hammer.DIRECTION_ALL };

  constructor(_hammerManager: HammerManager, _swipeConfig: RecognizerOptions) {
    this.hammerManager = _hammerManager;
    this.swipeConfig = { ...this.swipeConfig, ..._swipeConfig };

    this.setSwipeConfig();
    this.registerSwipe();
  }

  public get onSwipe(): Observable<SWIPE_ACTIONS> {
    return this.swipeObservable.asObservable();
  }

  private registerSwipe(): void {
    this.hammerManager.on('swipeleft', () => this.swipeLeft());
    this.hammerManager.on('swiperight', () => this.swipeRight());
    this.hammerManager.on('swipeup', () => this.swipeUp());
    this.hammerManager.on('swipedown', () => this.swipeDown());
  }

  private setSwipeConfig(): void {
    this.hammerManager.get('swipe').set(this.swipeConfig);
  }

  private pushSwipeAction(action: SWIPE_ACTIONS): void {
    this.swipeObservable.next(action);
  }

  private swipeLeft(): void {
    this.pushSwipeAction(SWIPE_ACTIONS.LEFT);
  }

  private swipeRight(): void {
    this.pushSwipeAction(SWIPE_ACTIONS.RIGHT);
  }

  private swipeUp(): void {
    this.pushSwipeAction(SWIPE_ACTIONS.UP);
  }

  private swipeDown(): void {
    this.pushSwipeAction(SWIPE_ACTIONS.DOWN);
  }
}