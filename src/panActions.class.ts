import { Observable, Subject } from 'rxjs';
import { SWIPE_ACTIONS } from './swipeAction.class';

export enum PAN_ACTIONS {
  START = 10,
  LEFT = 11,
  RIGHT = 12,
  UP = 13,
  DOWN = 14,
  END = 15,
  CANCEL = 16
}

export class PanActions {

  private panObservable: Subject<{ action: number, value: number }> = new Subject();
  private hammerManager: HammerManager;
  private panConfig: RecognizerOptions = { threshold: 0, direction: Hammer.DIRECTION_ALL };
  private container: HTMLElement;
  private lastPanAction: number = null;

  constructor(_hammerManager, _container: HTMLElement, _panConfig: RecognizerOptions) {
    this.hammerManager = _hammerManager;
    this.container = _container;
    this.panConfig = { ...this.panConfig, ..._panConfig };

    this.setPanConfig();
    this.registerPan();
  }

  public get onPan(): Observable<{ action: PAN_ACTIONS, value: number }> {
    return this.panObservable.asObservable();
  }

  private registerPan(): void {
    this.hammerManager.on('panstart', () => this.panStart());
    this.hammerManager.on('panleft', (event) => this.panLeft(event));
    this.hammerManager.on('panright', (event) => this.panRight(event));
    this.hammerManager.on('panup', (event) => this.panUp(event));
    this.hammerManager.on('pandown', (event) => this.panDown(event));
    this.hammerManager.on('panend pancancel', (event) => this.panEnd(event));
  }

  private setPanConfig(): void {
    this.hammerManager.get('pan').set(this.panConfig);
  }

  private pushPanAction(action: number, value: number = null) {
    this.panObservable.next({ action, value });
  }

  private setLastPanAction(action: PAN_ACTIONS): void {
    this.lastPanAction = action;
  }

  private panStart(): void {
    this.pushPanAction(PAN_ACTIONS.START);
  }

  private panLeft(event: HammerInput): void {
    if (!this.checkPanHorizontal(event)) return;

    this.pushPanAction(PAN_ACTIONS.LEFT, event.deltaX);
    this.setLastPanAction(PAN_ACTIONS.LEFT);
  }

  private panRight(event: HammerInput): void {
    if (!this.checkPanHorizontal(event)) return;

    this.pushPanAction(PAN_ACTIONS.RIGHT, event.deltaX);
    this.setLastPanAction(PAN_ACTIONS.RIGHT);
  }

  private panUp(event: HammerInput): void {
    if (!this.checkPanVertical(event)) return;

    this.pushPanAction(PAN_ACTIONS.UP, event.deltaY);
    this.setLastPanAction(PAN_ACTIONS.UP);
  }

  private panDown(event: HammerInput): void {
    if (!this.checkPanVertical(event)) return;

    this.pushPanAction(PAN_ACTIONS.DOWN, event.deltaY);
    this.setLastPanAction(PAN_ACTIONS.DOWN);
  }

  private panEnd(event: HammerInput): void {
    if (this.checkPanHorizontal(event) && this.checkPanHorizontalDistance(event.deltaX)) {
      if (this.lastPanAction === PAN_ACTIONS.LEFT) {
        this.pushPanAction(PAN_ACTIONS.CANCEL);
        this.pushPanAction(SWIPE_ACTIONS.LEFT);
        return;
      }
      if (this.lastPanAction === PAN_ACTIONS.RIGHT) {
        this.pushPanAction(PAN_ACTIONS.CANCEL);
        this.pushPanAction(SWIPE_ACTIONS.RIGHT);
        return;
      }
    }
    if (this.checkPanVertical(event) && this.checkPanVerticalDistance(event.deltaY)) {
      if (this.lastPanAction === PAN_ACTIONS.UP) {
        this.pushPanAction(PAN_ACTIONS.CANCEL);
        this.pushPanAction(SWIPE_ACTIONS.UP);
        return;
      }
      if (this.lastPanAction === PAN_ACTIONS.DOWN) {
        this.pushPanAction(PAN_ACTIONS.CANCEL);
        this.pushPanAction(SWIPE_ACTIONS.DOWN);
        return;
      }
    }

    if (this.checkPanHorizontal(event))
      this.pushPanAction(PAN_ACTIONS.END, event.deltaX);
    else if (this.checkPanVertical(event))
      this.pushPanAction(PAN_ACTIONS.END, event.deltaY);
    else
      this.pushPanAction(PAN_ACTIONS.CANCEL);
  }

  private checkPanHorizontal({ deltaX, deltaY, velocityX, velocityY }): boolean {
    if (Math.abs(deltaY) > Math.abs(deltaX)) return false;
    if (Math.abs(velocityY) > Math.abs(velocityX)) return false;

    return true;
  }

  private checkPanVertical({ deltaX, deltaY, velocityX, velocityY }): boolean {
    if (Math.abs(deltaY) < Math.abs(deltaX)) return false;
    if (Math.abs(velocityY) < Math.abs(velocityX)) return false;

    return true;
  }

  private checkPanHorizontalDistance(deltaX): boolean {
    const containerHalfWidth = this.container.clientWidth / 2;

    return Math.abs(deltaX) >= containerHalfWidth;
  }

  private checkPanVerticalDistance(deltaY): boolean {
    const containerHalfHeight = this.container.clientHeight / 2;

    return Math.abs(deltaY) >= containerHalfHeight;
  }
}