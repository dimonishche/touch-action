const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');
const cleanWebpackPlugin = require('clean-webpack-plugin');
const miniCssExtractPlugin = require('mini-css-extract-plugin');
const copyWebpackPlugin = require('copy-webpack-plugin');
const htmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const webpack = require('webpack');

module.exports = (env, argv) => {
  const devMode = argv.mode !== 'production';

  return {
    entry: {
      'app': './src/touchAction.class.ts'
    },
    output: {
      filename: '[name].bundle.js',
      path: path.resolve(__dirname, 'dist')
    },
    devtool: devMode ? 'inline-source-map' : '',
    devServer: {
      contentBase: './dist',
      hot: true,
      host: '192.168.0.35'
    },
    plugins: [
      new cleanWebpackPlugin(['dist']),
      // new htmlWebpackPlugin({
      //   template: './src/index.html',
      //   inlineSource: /\.css$/
      // }),
      // new htmlWebpackInlineSourcePlugin(),
      // new webpack.NamedModulesPlugin(),
      // new webpack.HotModuleReplacementPlugin(),
      // new miniCssExtractPlugin({
      //   filename: "[name].css",
      //   chunkFilename: "[id].css"
      // }),
      // new copyWebpackPlugin([{
      //   from: './src/assets',
      //   to: 'assets'
      // }])
    ],
    module: {
      rules: [
        // {
        //   test: /\.html$/,
        //   use: [{
        //     loader: 'html-loader',
        //     options: {
        //       interpolate: true,
        //       minimize: true
        //     }
        //   }]
        // },
        // {
        //   test: /\.(css)$/,
        //   use: [
        //     // devMode ? 'style-loader' : miniCssExtractPlugin.loader,
        //     miniCssExtractPlugin.loader,
        //     'css-loader',
        //     // 'postcss-loader'
        //   ]
        // },
        {
          test: /\.tsx?$/,
          use: 'ts-loader',
          exclude: /node_modules/
        }
        // {
        //   test: /\.js$/,
        //   use: 'babel-loader',
        //   // exclude: /node_modules/
        // }
      ]
    },
    resolve: {
      extensions: [ '.tsx', '.ts', '.js' ]
    }
  }
}