var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { Subject } from 'rxjs';
export var SWIPE_ACTIONS;
(function (SWIPE_ACTIONS) {
    SWIPE_ACTIONS[SWIPE_ACTIONS["LEFT"] = 0] = "LEFT";
    SWIPE_ACTIONS[SWIPE_ACTIONS["RIGHT"] = 1] = "RIGHT";
    SWIPE_ACTIONS[SWIPE_ACTIONS["UP"] = 2] = "UP";
    SWIPE_ACTIONS[SWIPE_ACTIONS["DOWN"] = 3] = "DOWN";
})(SWIPE_ACTIONS || (SWIPE_ACTIONS = {}));
var SwipeActions = /** @class */ (function () {
    function SwipeActions(_hammerManager, _swipeConfig) {
        this.swipeObservable = new Subject();
        this.swipeConfig = { velocity: 0.3, direction: Hammer.DIRECTION_ALL };
        this.hammerManager = _hammerManager;
        this.swipeConfig = __assign({}, this.swipeConfig, _swipeConfig);
        this.setSwipeConfig();
        this.registerSwipe();
    }
    Object.defineProperty(SwipeActions.prototype, "onSwipe", {
        get: function () {
            return this.swipeObservable.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    SwipeActions.prototype.registerSwipe = function () {
        var _this = this;
        this.hammerManager.on('swipeleft', function () { return _this.swipeLeft(); });
        this.hammerManager.on('swiperight', function () { return _this.swipeRight(); });
        this.hammerManager.on('swipeup', function () { return _this.swipeUp(); });
        this.hammerManager.on('swipedown', function () { return _this.swipeDown(); });
    };
    SwipeActions.prototype.setSwipeConfig = function () {
        this.hammerManager.get('swipe').set(this.swipeConfig);
    };
    SwipeActions.prototype.pushSwipeAction = function (action) {
        this.swipeObservable.next(action);
    };
    SwipeActions.prototype.swipeLeft = function () {
        this.pushSwipeAction(SWIPE_ACTIONS.LEFT);
    };
    SwipeActions.prototype.swipeRight = function () {
        this.pushSwipeAction(SWIPE_ACTIONS.RIGHT);
    };
    SwipeActions.prototype.swipeUp = function () {
        this.pushSwipeAction(SWIPE_ACTIONS.UP);
    };
    SwipeActions.prototype.swipeDown = function () {
        this.pushSwipeAction(SWIPE_ACTIONS.DOWN);
    };
    return SwipeActions;
}());
export { SwipeActions };
