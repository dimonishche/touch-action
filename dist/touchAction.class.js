// import 'hammerjs';
import { PanActions } from './panActions.class';
import { SwipeActions, SWIPE_ACTIONS } from './swipeAction.class';
import { Subject } from 'rxjs';
var hammerLoader = require('bundle-loader!hammerjs');
export var ACTIONS;
(function (ACTIONS) {
    ACTIONS[ACTIONS["SWIPE_LEFT"] = 0] = "SWIPE_LEFT";
    ACTIONS[ACTIONS["SWIPE_RIGHT"] = 1] = "SWIPE_RIGHT";
    ACTIONS[ACTIONS["SWIPE_UP"] = 2] = "SWIPE_UP";
    ACTIONS[ACTIONS["SWIPE_DOWN"] = 3] = "SWIPE_DOWN";
    ACTIONS[ACTIONS["PAN_START"] = 10] = "PAN_START";
    ACTIONS[ACTIONS["PAN_LEFT"] = 11] = "PAN_LEFT";
    ACTIONS[ACTIONS["PAN_RIGHT"] = 12] = "PAN_RIGHT";
    ACTIONS[ACTIONS["PAN_UP"] = 13] = "PAN_UP";
    ACTIONS[ACTIONS["PAN_DOWN"] = 14] = "PAN_DOWN";
    ACTIONS[ACTIONS["PAN_END"] = 15] = "PAN_END";
    ACTIONS[ACTIONS["PAN_CANCEL"] = 16] = "PAN_CANCEL";
})(ACTIONS || (ACTIONS = {}));
var TouchActionClass = /** @class */ (function () {
    function TouchActionClass(container, swipeConfig, panConfig, parent) {
        var _this = this;
        this.actionObservable = new Subject();
        this.readyState = new Subject();
        this.canSwipe = true;
        this.enabled = true;
        this.UNRESPONSE_TIME = 200;
        if (!window)
            return;
        this.container = container;
        this.parent = parent || this.container;
        hammerLoader(function (_hammer) {
            Hammer = _hammer;
            _this.createHammerObject();
            _this.readyState.next(true);
            _this.panActions = new PanActions(_this.hammerManager, _this.parent, panConfig);
            _this.swipeActios = new SwipeActions(_this.hammerManager, swipeConfig);
            _this.listenPan();
            _this.listenSwipe();
        });
    }
    Object.defineProperty(TouchActionClass.prototype, "actions", {
        get: function () {
            return this.actionObservable.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    TouchActionClass.prototype.disableActions = function () {
        try {
            this.hammerManager.set({ enable: false });
            this.enabled = false;
        }
        finally { }
    };
    TouchActionClass.prototype.enableActions = function () {
        this.hammerManager.set({ enable: true });
        this.enabled = true;
    };
    TouchActionClass.prototype.onReady = function () {
        return this.readyState.asObservable();
    };
    TouchActionClass.prototype.createHammerObject = function () {
        Hammer.defaults.cssProps.userSelect = 'text';
        this.hammerManager = new Hammer(this.container);
    };
    TouchActionClass.prototype.pushAction = function (action, value) {
        if (value === void 0) { value = null; }
        if (!this.canSwipe)
            return;
        if (action in SWIPE_ACTIONS)
            this.setSwipeUnresponse();
        this.actionObservable.next({ action: action, value: value });
    };
    TouchActionClass.prototype.listenPan = function () {
        var _this = this;
        this.panActions.onPan.subscribe(function (event) {
            _this.pushAction(event.action, event.value);
        });
    };
    TouchActionClass.prototype.listenSwipe = function () {
        var _this = this;
        this.swipeActios.onSwipe.subscribe(function (event) {
            _this.pushAction(event);
        });
    };
    TouchActionClass.prototype.setSwipeUnresponse = function () {
        var _this = this;
        this.canSwipe = false;
        setTimeout(function () { return _this.canSwipe = true; }, this.UNRESPONSE_TIME);
    };
    return TouchActionClass;
}());
export { TouchActionClass };
