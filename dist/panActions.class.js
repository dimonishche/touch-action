var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import { Subject } from 'rxjs';
import { SWIPE_ACTIONS } from './swipeAction.class';
export var PAN_ACTIONS;
(function (PAN_ACTIONS) {
    PAN_ACTIONS[PAN_ACTIONS["START"] = 10] = "START";
    PAN_ACTIONS[PAN_ACTIONS["LEFT"] = 11] = "LEFT";
    PAN_ACTIONS[PAN_ACTIONS["RIGHT"] = 12] = "RIGHT";
    PAN_ACTIONS[PAN_ACTIONS["UP"] = 13] = "UP";
    PAN_ACTIONS[PAN_ACTIONS["DOWN"] = 14] = "DOWN";
    PAN_ACTIONS[PAN_ACTIONS["END"] = 15] = "END";
    PAN_ACTIONS[PAN_ACTIONS["CANCEL"] = 16] = "CANCEL";
})(PAN_ACTIONS || (PAN_ACTIONS = {}));
var PanActions = /** @class */ (function () {
    function PanActions(_hammerManager, _container, _panConfig) {
        this.panObservable = new Subject();
        this.panConfig = { threshold: 0, direction: Hammer.DIRECTION_ALL };
        this.lastPanAction = null;
        this.hammerManager = _hammerManager;
        this.container = _container;
        this.panConfig = __assign({}, this.panConfig, _panConfig);
        this.setPanConfig();
        this.registerPan();
    }
    Object.defineProperty(PanActions.prototype, "onPan", {
        get: function () {
            return this.panObservable.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    PanActions.prototype.registerPan = function () {
        var _this = this;
        this.hammerManager.on('panstart', function () { return _this.panStart(); });
        this.hammerManager.on('panleft', function (event) { return _this.panLeft(event); });
        this.hammerManager.on('panright', function (event) { return _this.panRight(event); });
        this.hammerManager.on('panup', function (event) { return _this.panUp(event); });
        this.hammerManager.on('pandown', function (event) { return _this.panDown(event); });
        this.hammerManager.on('panend pancancel', function (event) { return _this.panEnd(event); });
    };
    PanActions.prototype.setPanConfig = function () {
        this.hammerManager.get('pan').set(this.panConfig);
    };
    PanActions.prototype.pushPanAction = function (action, value) {
        if (value === void 0) { value = null; }
        this.panObservable.next({ action: action, value: value });
    };
    PanActions.prototype.setLastPanAction = function (action) {
        this.lastPanAction = action;
    };
    PanActions.prototype.panStart = function () {
        this.pushPanAction(PAN_ACTIONS.START);
    };
    PanActions.prototype.panLeft = function (event) {
        if (!this.checkPanHorizontal(event))
            return;
        this.pushPanAction(PAN_ACTIONS.LEFT, event.deltaX);
        this.setLastPanAction(PAN_ACTIONS.LEFT);
    };
    PanActions.prototype.panRight = function (event) {
        if (!this.checkPanHorizontal(event))
            return;
        this.pushPanAction(PAN_ACTIONS.RIGHT, event.deltaX);
        this.setLastPanAction(PAN_ACTIONS.RIGHT);
    };
    PanActions.prototype.panUp = function (event) {
        if (!this.checkPanVertical(event))
            return;
        this.pushPanAction(PAN_ACTIONS.UP, event.deltaY);
        this.setLastPanAction(PAN_ACTIONS.UP);
    };
    PanActions.prototype.panDown = function (event) {
        if (!this.checkPanVertical(event))
            return;
        this.pushPanAction(PAN_ACTIONS.DOWN, event.deltaY);
        this.setLastPanAction(PAN_ACTIONS.DOWN);
    };
    PanActions.prototype.panEnd = function (event) {
        if (this.checkPanHorizontal(event) && this.checkPanHorizontalDistance(event.deltaX)) {
            if (this.lastPanAction === PAN_ACTIONS.LEFT) {
                this.pushPanAction(PAN_ACTIONS.CANCEL);
                this.pushPanAction(SWIPE_ACTIONS.LEFT);
                return;
            }
            if (this.lastPanAction === PAN_ACTIONS.RIGHT) {
                this.pushPanAction(PAN_ACTIONS.CANCEL);
                this.pushPanAction(SWIPE_ACTIONS.RIGHT);
                return;
            }
        }
        if (this.checkPanVertical(event) && this.checkPanVerticalDistance(event.deltaY)) {
            if (this.lastPanAction === PAN_ACTIONS.UP) {
                this.pushPanAction(PAN_ACTIONS.CANCEL);
                this.pushPanAction(SWIPE_ACTIONS.UP);
                return;
            }
            if (this.lastPanAction === PAN_ACTIONS.DOWN) {
                this.pushPanAction(PAN_ACTIONS.CANCEL);
                this.pushPanAction(SWIPE_ACTIONS.DOWN);
                return;
            }
        }
        if (this.checkPanHorizontal(event))
            this.pushPanAction(PAN_ACTIONS.END, event.deltaX);
        else if (this.checkPanVertical(event))
            this.pushPanAction(PAN_ACTIONS.END, event.deltaY);
        else
            this.pushPanAction(PAN_ACTIONS.CANCEL);
    };
    PanActions.prototype.checkPanHorizontal = function (_a) {
        var deltaX = _a.deltaX, deltaY = _a.deltaY, velocityX = _a.velocityX, velocityY = _a.velocityY;
        if (Math.abs(deltaY) > Math.abs(deltaX))
            return false;
        if (Math.abs(velocityY) > Math.abs(velocityX))
            return false;
        return true;
    };
    PanActions.prototype.checkPanVertical = function (_a) {
        var deltaX = _a.deltaX, deltaY = _a.deltaY, velocityX = _a.velocityX, velocityY = _a.velocityY;
        if (Math.abs(deltaY) < Math.abs(deltaX))
            return false;
        if (Math.abs(velocityY) < Math.abs(velocityX))
            return false;
        return true;
    };
    PanActions.prototype.checkPanHorizontalDistance = function (deltaX) {
        var containerHalfWidth = this.container.clientWidth / 2;
        return Math.abs(deltaX) >= containerHalfWidth;
    };
    PanActions.prototype.checkPanVerticalDistance = function (deltaY) {
        var containerHalfHeight = this.container.clientHeight / 2;
        return Math.abs(deltaY) >= containerHalfHeight;
    };
    return PanActions;
}());
export { PanActions };
