/// <reference types="hammerjs" />
import { Observable } from 'rxjs';
export declare enum PAN_ACTIONS {
    START = 10,
    LEFT = 11,
    RIGHT = 12,
    UP = 13,
    DOWN = 14,
    END = 15,
    CANCEL = 16
}
export declare class PanActions {
    private panObservable;
    private hammerManager;
    private panConfig;
    private container;
    private lastPanAction;
    constructor(_hammerManager: any, _container: HTMLElement, _panConfig: RecognizerOptions);
    readonly onPan: Observable<{
        action: PAN_ACTIONS;
        value: number;
    }>;
    private registerPan;
    private setPanConfig;
    private pushPanAction;
    private setLastPanAction;
    private panStart;
    private panLeft;
    private panRight;
    private panUp;
    private panDown;
    private panEnd;
    private checkPanHorizontal;
    private checkPanVertical;
    private checkPanHorizontalDistance;
    private checkPanVerticalDistance;
}
