import { Observable } from 'rxjs';
export interface SwipeConfig {
    velocity: number;
    direction: number;
}
export interface PanConfig {
    threshold: number;
    direction: number;
}
export declare enum ACTIONS {
    SWIPE_LEFT = 0,
    SWIPE_RIGHT = 1,
    SWIPE_UP = 2,
    SWIPE_DOWN = 3,
    PAN_START = 10,
    PAN_LEFT = 11,
    PAN_RIGHT = 12,
    PAN_UP = 13,
    PAN_DOWN = 14,
    PAN_END = 15,
    PAN_CANCEL = 16
}
export declare class TouchActionClass {
    private actionObservable;
    private readyState;
    private container;
    private parent;
    private hammerManager;
    private panActions;
    private swipeActios;
    private canSwipe;
    private enabled;
    private readonly UNRESPONSE_TIME;
    constructor(container: HTMLElement, swipeConfig?: SwipeConfig, panConfig?: PanConfig, parent?: HTMLElement);
    readonly actions: Observable<{
        action: ACTIONS;
        value: number;
    }>;
    disableActions(): void;
    enableActions(): void;
    onReady(): Observable<boolean>;
    private createHammerObject;
    private pushAction;
    private listenPan;
    private listenSwipe;
    private setSwipeUnresponse;
}
