/// <reference types="hammerjs" />
import { Observable } from 'rxjs';
export declare enum SWIPE_ACTIONS {
    LEFT = 0,
    RIGHT = 1,
    UP = 2,
    DOWN = 3
}
export declare class SwipeActions {
    private swipeObservable;
    private hammerManager;
    private swipeConfig;
    constructor(_hammerManager: HammerManager, _swipeConfig: RecognizerOptions);
    readonly onSwipe: Observable<SWIPE_ACTIONS>;
    private registerSwipe;
    private setSwipeConfig;
    private pushSwipeAction;
    private swipeLeft;
    private swipeRight;
    private swipeUp;
    private swipeDown;
}
